/*
This class is intened to create a massive amount of objects to
represent/see the automatic garbage collector in action.
*/
class GarbageDemo
{
	public static void main(String args[])
	{
		int count;
		
		System.out.println("Before the first object creation");

		//Create an object of class FDemo and pass 0 to the constructor.
		FDemo ob = new FDemo(0);

		System.out.println("Before for loop");

		//Generate a large numer of objects at some point the garbage collector will come in and destroy objects.
		for(count = 1; count <= 10000000; count++)
		{
			ob.generator(count);
		}

		System.out.println("After for loop");
	}
}

/*
This class is a part of the automatic garbage colletor demonstration.
Class caller : GarbageCollector.java
*/

class FDemo
{
	int x;

	FDemo(int i)
	{
		x = i;
	}

	//Called when an object is recycled
	protected void finalize()
	{
		System.out.println("Destroying object: " + x);
	}

	void generator(int i)
	{
		FDemo o = new FDemo(i);
	}
}
