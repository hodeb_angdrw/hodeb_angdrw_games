package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

public class MyGdxGame extends ApplicationAdapter {
    private OrthographicCamera camera;
    private SpriteBatch batch;

    private Texture bgSprite;
    private Texture alienSprite;
    private Texture playerSprite;
    private Texture bulletSprite;
    private Texture enemyBulletSprite;

    static Player player;
    static Alien alien;

    private static Array<Bullet> bulletList;

    private int width;
    private int height;

    @Override
    public void create() {
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 600);
        batch = new SpriteBatch();

        // loading textures
        bgSprite = new Texture("bg.png");
        alienSprite = new Texture("enemy.png");
        playerSprite = new Texture("spaceship.png");
        bulletSprite = new Texture("bullet.png");
        enemyBulletSprite = new Texture("enemybullet.png");

        // declaring objects
        player = new Player(width / 2 - playerSprite.getWidth() / 2,
                            30, playerSprite);

        alien = new Alien(width / 2 - alienSprite.getWidth() / 2,
                          height - 30 - alienSprite.getHeight(),
                          alienSprite);

        bulletList = new Array<Bullet>();
    }

    static void shoot(int x, int y, int dir, Texture sprite) {
        Bullet bullet = new Bullet(x, y, dir, sprite);
        bulletList.add(bullet);
    }

    private void loop() {
        player.input();
        player.update();

        // alien AI
        alien.move_to(player.x, player.y);
        alien.update();

        Bullet.handle_bullets(bulletList);
    }

    @Override
    public void render() {
        // clearing
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        // main rendering
        batch.draw(bgSprite, 0, 0);
        batch.draw(player.sprite, player.x, player.y);
        batch.draw(alien.sprite, alien.x, alien.y);

        for(Bullet bullet: bulletList){
            batch.draw(bullet.sprite, bullet.x, bullet.y);
        }

        batch.end();

        loop();
    }
}
