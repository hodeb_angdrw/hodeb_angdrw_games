package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

class Alien extends Rectangle {

    Texture sprite;
    private Texture bulletSprite;

    private int speed;
    int health;

    Alien(int x, int y, Texture sprite) {
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        this.width = sprite.getWidth();
        this.height = sprite.getHeight();
        this.speed = 200;
        this.health = 100;

        bulletSprite = new Texture("enemybullet.png");
    }

    void move_to(float x, float y) {
        //moves towards supplied location
        if(this.x < x) this.x += this.speed * Gdx.graphics.getDeltaTime();
        if(this.x > x) this.x -= this.speed * Gdx.graphics.getDeltaTime();
    }

    void update() {
        //shoot randomly
        if(MathUtils.random(0,100) > 95) {
            MyGdxGame.shoot(Math.round(this.x + this.width / 2 - 5),
                            Math.round(this.y - 10),
                            -1, bulletSprite);
        }

        if(this.health < 0){
            System.out.println("Alien dead");
        }
    }

}
