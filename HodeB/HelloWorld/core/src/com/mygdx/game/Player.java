package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

class Player extends Rectangle {

    Texture sprite;
    private Texture bulletSprite;
    int health;

    Player(int x, int y, Texture sprite) {
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        this.width = sprite.getWidth();
        this.height = sprite.getHeight();

        bulletSprite = new Texture("bullet.png");

        this.health = 100;
    }

    void input() {
        if(Gdx.input.isKeyPressed(Input.Keys.A)) this.x -= 300 * Gdx.graphics.getDeltaTime();
        if(Gdx.input.isKeyPressed(Input.Keys.D)) this.x += 300 * Gdx.graphics.getDeltaTime();

        // shooting
        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            // spawn a bullet 10 pixels above the middle of the player going upwards
            MyGdxGame.shoot(Math.round(this.x + this.getWidth() / 2 - 5),
                  Math.round(this.y + this.getHeight() + 10),
                  1, bulletSprite);
        }
    }

    void update() {
        // making the player stay on screen
        if(this.x < 0) this.x = 0;
        if(this.x + this.width > Gdx.graphics.getWidth()) this.x = width - this.width;

        if(this.health < 0){
            System.out.println("Player dead");
        }

    }

}
