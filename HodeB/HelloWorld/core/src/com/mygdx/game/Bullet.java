package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

class Bullet extends Rectangle {

    Texture sprite;
    private boolean active;
    private int dir;
    private int damage;

    Bullet(int x, int y, int direction, Texture sprite) {
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        this.dir = direction;

        this.width = sprite.getWidth();
        this.height = sprite.getHeight();
        this.active = true;

        this.damage = 10;
    }

    private void update() {
        // move the bullet
        this.y += this.dir * 400 * Gdx.graphics.getDeltaTime();

        // if the bullet is out of the screen delete it
        if(this.y < 0 || this.y > Gdx.graphics.getHeight()) this.active = false;

        // checking if the bullet collides with an alien or the player
        if(this.overlaps(MyGdxGame.player)){
            MyGdxGame.player.health -= this.damage;
            this.active = false;
        }
        if(this.overlaps(MyGdxGame.alien)){
            MyGdxGame.alien.health -= this.damage;
            this.active = false;
        }
    }

    static void handle_bullets(Array<Bullet> list) {
        Iterator<Bullet> iter = list.iterator();
        while(iter.hasNext()) {
            Bullet bullet = iter.next();
            bullet.update();
            if(!bullet.active) iter.remove();
        }

    }

}
